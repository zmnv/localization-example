import React from 'react';
import { useTranslation } from 'react-i18next';

export const Provisioning = (props) => {
    const { t, i18n } = useTranslation('provisioning', { useSuspense: false });

    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
    };

    return (
        <div style={{ marginBottom: 48 }}>
            <h4>Provisioning</h4>
            <p>{t('add_new')}</p>
            <p>{t('error', { context: 'offline' })}</p>
            <p>{t('error', { context: 'access-deny' })}</p>
        </div>
    )
}
