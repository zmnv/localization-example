import en from './en.json';
import ru from './ru.json';

export const provisioningLocales = (i18next) => {
    i18next.addResourceBundle('en', 'provisioning', en);
    i18next.addResourceBundle('ru', 'provisioning', ru);
};
