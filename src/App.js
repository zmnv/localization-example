import React from 'react';
import './App.css';

import { Calibration } from './Calibration';
import { Provisioning } from './Provisioning';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Calibration />
        <Provisioning />
      </header>
    </div>
  );
}

export default App;
