import i18next from 'i18next';
// import LngDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import i18nextOptions from './i18nextOptions';

import { calibrationLocales } from '../Calibration/locales';
import { provisioningLocales } from '../Provisioning/locales';


const i18n = i18next.createInstance();

i18n
    // .use(LngDetector)
    .use(initReactI18next)
    .init(i18nextOptions())
;

calibrationLocales(i18n);
provisioningLocales(i18n);

export default i18n;