import React from 'react';
import { useTranslation } from 'react-i18next';


export const Calibration = (props) => {
    const { t, i18n } = useTranslation('calibration', { useSuspense: false });

    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
    };

    return (
        <div style={{ marginBottom: 48 }}>
            <h4>Calibration</h4>
            <p>{t('hello')}</p>
            <p>{t('nested_phrases', { user: 'Valeriy', level: 99 })}</p>
            <button onClick={() => changeLanguage('ru')}>ru</button>
            <button onClick={() => changeLanguage('en')}>en</button>
        </div>
    )
}
