import en from './en.json';
import ru from './ru.json';

export const calibrationLocales = (i18next) => {
    i18next.addResourceBundle('en', 'calibration', en);
    i18next.addResourceBundle('ru', 'calibration', ru);
};
